json.array!(@companies) do |company|
  json.extract! company, :id, :name, :photo, :description
  json.url company_url(company, format: :json)
end
