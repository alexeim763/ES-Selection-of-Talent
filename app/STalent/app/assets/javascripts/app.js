var app = angular.module('myApp' , ['ngAnimate']);

app.controller('menuMobileController', function(){
	this.show = false;
	this.toggle = function(){
		console.log(this.show);
		this.show = this.show ? false : true;
	};
});

app.controller('jobsController', function(){
	this.show = false;
	this.toggle = function(){
		console.log(this.show);
		this.show = this.show ? false : true;
	};
});

$(document).on('ready page:load', function(arguments) {
  angular.bootstrap(document.body, ['myApp'])
});

