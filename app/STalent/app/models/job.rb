class Job < ActiveRecord::Base
	validates :title, presence: true
	validates :description, presence: true, length: { minimum: 20 }
	validates :minimumEducation, presence: true
	validates :yearExperience, presence: true
	validates :lenguages, presence: true
	belongs_to :company
end
