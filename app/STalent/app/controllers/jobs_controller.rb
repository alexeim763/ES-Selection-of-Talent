class JobsController < ApplicationController
	layout 'dashboard'
	before_action :set_company, except: [:all]
	before_action :authenticate_user! , except: [:all,:index,:show]
	#GET /jobs/all

	def all
		@jobs = Job.all
	end
	#GET /jobs
	def index
		@jobs = Job.all
	end

	#GET /jobs/:id
	def show
		@job = Job.find(params[:id])
	end

	def new
		@job = Job.new()


	end
	#POST /jobs
	def create
		@job = Job.new(job_params)
		@job.company = @company
		if @job.save
			redirect_to @job.company
		else
			render :new
		end

	end

	#PUT /jobs/:id
	def update
		@job = Job.find(params[:id])
		if @job.update(job_params)
			redirect_to @job.company
		else
			render :edit
		end
	end


	def edit
		@job = Job.find(params[:id])
	end

	def destroy
		@job = Job.find(params[:id])
		@job.destroy
		redirect_to @company
	end

	private
	def set_company
		@company = Company.find(params[:company_id])
	end
	def job_params
		params.require(:job).permit(:title , :salary, :description , :recruitmentTime, :minimumEducation ,:yearExperience ,:lenguages ,:other)
	end

end