# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150716142606) do

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "photo"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "computer_skills", force: :cascade do |t|
    t.string   "name"
    t.integer  "curriculum_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "computer_skills", ["curriculum_id"], name: "index_computer_skills_on_curriculum_id"

  create_table "curriculums", force: :cascade do |t|
    t.integer  "dni"
    t.date     "dateBirth"
    t.string   "maritalStatus"
    t.integer  "phone"
    t.string   "country"
    t.string   "deparment"
    t.string   "city"
    t.integer  "postalCode"
    t.string   "address"
    t.string   "nationality"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "user_id"
  end

  add_index "curriculums", ["user_id"], name: "index_curriculums_on_user_id"

  create_table "educations", force: :cascade do |t|
    t.string   "institution"
    t.string   "levelEducation"
    t.string   "state"
    t.date     "iPeriod"
    t.date     "ePeriod"
    t.integer  "curriculum_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "educations", ["curriculum_id"], name: "index_educations_on_curriculum_id"

  create_table "jobs", force: :cascade do |t|
    t.string   "title"
    t.integer  "salary"
    t.text     "description"
    t.integer  "recruitmentTime"
    t.string   "minimumEducation"
    t.integer  "yearExperience"
    t.string   "lenguages"
    t.text     "other"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "company_id"
  end

  add_index "jobs", ["company_id"], name: "index_jobs_on_company_id"

  create_table "lenguages", force: :cascade do |t|
    t.string   "name"
    t.string   "level"
    t.integer  "curriculum_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "lenguages", ["curriculum_id"], name: "index_lenguages_on_curriculum_id"

  create_table "profesional_experiences", force: :cascade do |t|
    t.string   "company"
    t.string   "region"
    t.string   "companyIndustry"
    t.string   "position"
    t.string   "area"
    t.date     "startWork"
    t.date     "endWork"
    t.text     "functions"
    t.integer  "curriculum_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "profesional_experiences", ["curriculum_id"], name: "index_profesional_experiences_on_curriculum_id"

  create_table "profesional_profiles", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "curriculum_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "profesional_profiles", ["curriculum_id"], name: "index_profesional_profiles_on_curriculum_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "name"
    t.string   "permission_level"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
