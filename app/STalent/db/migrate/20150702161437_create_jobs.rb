class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.integer :salary
      t.text :description
      t.integer :recruitmentTime
      t.string :minimumEducation
      t.integer :yearExperience
      t.string :lenguages
      t.text :other

      t.timestamps null: false
    end
  end
end


