var gulp = require('gulp');
var stylus = require('gulp-stylus');
var connect = require('gulp-connect');
var jade = require('gulp-jade');

gulp.task('css', function(){
	gulp.src('./lib/style.styl')
	.pipe(stylus({compress: true}))
	.pipe(gulp.dest('./public/css/'))
	.pipe(connect.reload());
});

gulp.task('jade', function() {
	gulp.src('./lib/*.jade')
	.pipe(jade({
		pretty: true
	}))
	.pipe(gulp.dest('./public/'))
	.pipe(connect.reload());
});

gulp.task('watch', function(){
	gulp.watch('./lib/*.jade' , ['jade']),
	gulp.watch('./lib/style.styl', ['css'])
	
});

gulp.task('webserver' , function(){
	connect.server({
		root: './public/',
		hostname: '0,0,0,0',
		port: 9000,
		livereload: true
	});
});

gulp.task('default', ['webserver','css','watch', 'jade']);